tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer scopeID = 0;
}
prog    : (e+=zakres | e+=expr | d+=declLocal)* -> program(name={$e},deklaracje={$d});

zakres : ^(BEGIN  {scopeID=enterScope();} (e+=zakres | e+=expr | d+=declLocal)* {scopeID=leaveScope();}) -> blok(wyr={$e}, dekl={$d});

//decl  :
//        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
//    ;
//    catch [RuntimeException ex] {errorID(ex,$i1);}
    
declLocal :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dekLocal(n={$ID.text}, s={scopeID.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.text+scopeID.toString()},p2={$e2.text+scopeID.toString()})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.text+scopeID.toString()},p2={$e2.text+scopeID.toString()})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.text+scopeID.toString()},p2={$e2.text+scopeID.toString()})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.text+scopeID.toString()},p2={$e2.text+scopeID.toString()})
        | ^(PODST i1=ID   e2=expr) {locals.setSymbol($i1.text, Integer.parseInt($e2.text));} -> podstaw(p1={$i1.text+scopeID.toString()},p2={$e2.text}) 
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
        | ID          {getSymbol($ID.text);} -> id(n={$ID.text+scopeID.toString()})
    ;
    